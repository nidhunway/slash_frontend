import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../partials/partials.module';

import { JournalVoucherComponent } from './journal-voucher/journal-voucher.component';
import { AccountsRoutingModule } from './accounts-routing.module';
import { EditJournalComponent } from './edit-journal/edit-journal.component';

import { JournalModalComponent } from './journal-modal/journal-modal.component';
import { ModalModule } from '../modal/modal.module';
@NgModule({
  imports: [
    CommonModule,
    AccountsRoutingModule,
    PartialsModule,
    ModalModule,
    NgbModule.forRoot()
  ],
  declarations: [
  JournalVoucherComponent,
  EditJournalComponent,
  JournalModalComponent
  ],
  entryComponents: [
    JournalModalComponent
  ],
  providers: [
    NgbActiveModal
  ]
})

export class AccountsModule { }
