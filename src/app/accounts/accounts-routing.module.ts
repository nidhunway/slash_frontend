import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { JournalVoucherComponent } from './journal-voucher/journal-voucher.component';
import { AuthGuardService } from '../auth-guard.service';

const journalRoutes: Routes = [
  {
    path: 'journal-voucher',
    component: JournalVoucherComponent,
    canActivate: [AuthGuardService]
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(journalRoutes)
  ],
  exports: [
  	RouterModule
  ],
  declarations: []
})
export class AccountsRoutingModule { }
