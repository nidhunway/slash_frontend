import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from '../../config/config.service';
// import { ModalComponent } from '../modal/modal.component';
import { JournalModalComponent } from '../journal-modal/journal-modal.component';

@Component({
  selector: 'app-journal-voucher',
  templateUrl: './journal-voucher.component.html',
  providers:[ConfigService,NgbModal],
  styleUrls: ['./journal-voucher.component.css']
})
export class JournalVoucherComponent implements OnInit {
	title='Journal Voucher';

	private journal_vouchers:Array<object>=[];

  constructor(private configService: ConfigService,private ngbModal:NgbModal) { }

  ngOnInit() {
  	this.getJournalVoucher();
  }
  getJournalVoucher(){

  	  	this.configService.GetJournalVoucher().subscribe((data:  Array<object>) => {
       

          this.journal_vouchers=data;



    });

  }
  edit(){

      // console.log(this.ngbModule);
      const modal=this.ngbModal.open(JournalModalComponent);
      modal.componentInstance.title='Modal';
  }
}
