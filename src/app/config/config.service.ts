import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'token':  '01eb9081-b32f-449a-9ae3-d8aca494863c'
  })
};

@Injectable({
  providedIn: 'root'
})

// headers.set('token','01eb9081-b32f-449a-9ae3-d8aca494863c');

export class ConfigService {

	API_URL='http://stashinvsa-001-site2.etempurl.com/api';

  constructor(private http: HttpClient) { }

  GetJournalVoucher(){
  	return this.http.get(`${this.API_URL}/JournalVoucher/GetJournalVoucher`,httpOptions);
  }
}
