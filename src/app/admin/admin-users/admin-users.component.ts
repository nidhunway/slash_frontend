import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config/config.service';
@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  providers:[ConfigService],
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {
  title='Users';
	private users:Array<object>=[];

  constructor(private configService: ConfigService) { }

  ngOnInit() {
  	this.getAllUsers();
  }
  getAllUsers(){

  }
}
