import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {AdminUsersComponent} from './admin-users/admin-users.component';
import { AuthGuardService } from '../auth-guard.service';
const adminRoutes: Routes = [
  {
    path: 'admin-users',
    component: AdminUsersComponent,
    canActivate: [AuthGuardService]
/*    ,children: [
      {
        path: '',
        children: [
          { path: 'crises', component: ManageCrisesComponent },
          { path: 'heroes', component: ManageHeroesComponent },
          { path: '', component: AdminDashboardComponent }
        ]
      }
    ]*/
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(adminRoutes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class AdminRoutingModule { }
