import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { HeaderComponent } from '../partials/header/header.component';
/*import { FooterComponent } from '../partials/footer/footer.component';
import { SidebarComponent } from '../partials/sidebar/sidebar.component';
import { NavbarComponent } from '../partials/navbar/navbar.component';*/
import { PartialsModule } from '../partials/partials.module';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminRoutingModule } from './admin-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    PartialsModule
  ],
  declarations: [
  AdminUsersComponent/*,
  FooterComponent,
  SidebarComponent,
  NavbarComponent*/
  ],
  exports: [AdminUsersComponent]
})
export class AdminModule { }
